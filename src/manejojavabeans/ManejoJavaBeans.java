package manejojavabeans;

import beans.PersonaBean;

/**
 *
 * @author tinix
 */
public class ManejoJavaBeans {

    public static void main(String[] args) {
       
        PersonaBean bean = new PersonaBean();
        bean.setNombre("Tinix");
        bean.setEdad(29);
        
        System.out.println("Nombre : "+ bean.getNombre());
        System.out.println("Edad : " + bean.getEdad());
    }    
}


